# Documentation :star2:

#### SOMMAIRE
  - [Le workflow avec git et gitlab dans vos différents projets.](#le-workflow-avec-git-et-gitlab-dans-vos-différents-projets.)
  - [Pusher vôtre code](#pusher-mon-code)
  - [Faire valider son travail]()
  - [Comment Faire valider son travail : cela passe par Pull Pequest](#faire-valider-son-travail)
  - [Corrections suivant les commentaires fait sur un PR]()
  - [Gestion des conflits]()
  - [Contributeurs]()
  - [Définition de termes]()

### Prérequis : 
  - Etre à l’aise avec [Git](https://git-scm.com/)
  - Connaitre [GitLab](https://gitlab.com/)
  - avoir yarn installé (optionnel)
  - Etre un Akiller[^akiller]

Grand merci à tous. Ce document est la matérialisation de ce que j'ai compris à travers les réponses que vous donnez à mes questions
### :one: Le workflow avec git et gitlab dans vos différents projets.

Si vous êtes déjà un habitué, ce document pourrait s'averer moins utile. Dans le cas contraire, vous êtes au bon endroit.

Ce que vous devez retenir est que pour faire d'un projet le vôtre, vous pouvez le forker ou le cloner.
Supposons que vous êtes déja ajouté au projet de base(lorsqu'il est priveé). Pour forker le projet, cliquez sur le bouton `fork` [`avec l'icone en Y`] du projet en question comme le montre l'image ci-contre

![Forker un projet!](/img/forker-1.png "Cliquez sur le bouton avec l'icone en Y")

Vous verrez apparaitre le formulaire suivant. Remplissez-le puis cliquez sur `fork project` et le projet fera partie de vos projets désormais.

![Forker un projet!](/img/forker-2.png "Cliquez sur le bouton avec l'icone en Y")

> ##### Vous pouvez maintenant cloner le projet afin de l’avoir localement

```ts
git clone origin [lien de vôtre dépot]
```

> ###### _Dans vôtre terminal, entrez le commande ci-dessus_.

Quand vous forkez un projet, il vient avec une branche, (master) par défaut. C’est donc à partir de là, que vous allez commencer à travailler.

> ##### Vous devez lier maintenant vôtre dépôt local à celui que vous avez forker.

```ts
git remote add upstream [lien du dépôt de base (que vous avez forker)]
```
>  ###### *Dans vôtre terminal, entrez le commande ci-dessus*.
> ##### Voir les connexions distantes que vous avez avec d'autres dépôt.

```ts
git remote -v
```

### NB : Pour chaque fonctionnalité à implémenter, vous devez créer une nouvelle `branche`

Le projet vient avec la branche master vous avais-je dis. D'emblée, à partir de la master, créez une nouvelle branche : `fonctionnalite1`. Ensuite déplacez-vous dans cette nouvelle branche. Travaillez

> ##### Créer une branche et y basculer

```ts
git checkout -b fonctionnalite1
```

> ##### Travailler et après

```ts
git add .
git commit -m "`message du commit`"
```

Avant de soumettre vôtre travail, n'oubliez pas d'incrémenter la version de vôtre code. le versioning va permettre à git de suivre vôtre progression et/ou de vous vous retrouvez vous-même.
>  ###### *Dans vôtre terminal*. *entrez la commande suivante*

```ts
yarn build:version
```

### :two: Pusher mon code (sur vôtre dépôt)

Une fois que vous avez terminé ce que vous avez à faire, `pusher` vôtre travail sur vôtre dépôt distant. Et avant de pusher rassurez-vous de récupérer ce qui est sur la branche master du projet de base. 

```ts
git pull --rebase upstream master
```

>  ###### *Dans vôtre terminal, entrez la commande ci-dessus*.

Et quand tout est bon, faites
```ts
git push -f origin `fonctionnalite1`
```

<!-- #### Important : vous devez encadrer chacun de vos push par deux pull rebase. Nous verrons ceci un peu plus tard Voir  -->
### :three: Faire valider son travail

Une autre chose à savoir est qu’ici (`À Akil`), ce n'est pas à vous de valider vôtre propre travail. À moins que vous soyez un `Master`[^master]. Et le processus de validation de vôtre travail passe par un `merge request (MR)`. Ne vous inquietez pas quand vous entendrez parler de `PR`. Il s'agit en fait de `Pull Request`. C'est la même chose. Cela depend du fait que vous êtes sur `github` ou `gitlab`

### :four: Comment faire valider son travail : cela passe par Pull Request (`PR`) ?

Après un `push`, git vous fournit un lien qui ressemble à peu près à ce qui suit :
remote : https://gitlab.com/userName/projectName/-/merge_requests/new?merge_request%5Bsource_branch%5D=branchName

![Faire un PR!](/img/pr.png "Cliquez sur le lien")

Cliquez sur ce lien puis renseignez les informations demandées. Ces informations concernent `Assignee` (le nom d'utilisateur github/gitlab de celui à qui vous vous assignez le PR; probablement vous-même), `Reviewer`(le nom d'utilisateur github/gitlab de vôtre Master)... `Milestone`, `Labels` peuvent rester par défaut. Une fois renseignées, cliquez sur le bouton `create merge request`. Attendez donc que vôtre Master valide vôtre travail ou qu'il le critique ou qu'il vous propose une amélioration.

### :warning: Sachez qu' un Master a beaucoup à faire. :warning: 
Donc en attendant qu’il valide vôtre travail, vous devez continuer de travailler. Créez donc une nouvelle branche `fonctionnalite2` depuis la branche `fonctionnalite1`. Continuez de travailler *`git add . && git commit -m "message du commit")`* comme d’habitude.

##### Lorsque vous allez vouloir faire valider vôtre travail, c’est la branche `fonctionnalite2` que vous devez push cette fois-ci...  (*Cf :two: et :four:*)
##### Retenz que celle-ci est la plus à jour.

NB : lorsque la fonctionnalité à implémenter dans la branche `fonctionnalite2` dépend de la branche `fonctionnalite1` créez donc la nouvelle branche à partir de l’ancienne. Dans le cas contraire revenez dans la branche `master` grâce à un checkout.

NB : continuez !!!, créez une nouvelle branche, travaillez, pusher puis faites un PR. Il peut arriver que vous ayez jusqu'à 8, 10 PR en attente. Ce n'est pas grave :smile:. Rappelez à chaque fois vôtre Master et continuer :computer:

### :five: Gestion des conflits

Il peut arriver que vôtre travail ressemble à ceci : De vôtre branche master vous avez créé une première branche (vous avez push et fait un PR), une deuxième branche depuis la première (que vous avez pushé et fait un PR également). Puis une troisième, une quatrième...une n-ième. En attendant que le master merge ou vous fasse un retour pour d'éventuelles corrections, créez une (n-ième + 1) (depuis la n-ième) dans laquelle vous allez continuer de travailler.

Supposons que chemin faisant, il vous fait un retour pour le premier PR et un quelconque autre, vous avez deux choix qui s'offrent a vous (ces deux choix consistent tous à mettre en attente ce que vous êtes entrain de faire) :

  - #####  *Faire un PR anticipé et le mettant au brouillon*
  - ##### *Mettre en cache vos fichiers modifiés et ajoutés*

##### :heavy_minus_sign: Faire un PR anticipé

Faites `git add . && git commit`... Vous connaissez la chanson maintenant, je suppose. Si non, Cf :two: et :four:. Ce PR a la possibilité de prendre un état `brouillon (WIP)`[^wip] qui veut dire que vous continuez de travailler. Ainsi, gitlab va désactiver le bouton qui permet à vôtre Master de merger. Il pourra certes voir ce que vous avez fait et c’est tout.

Dès que vous finissez de corriger, revenez dans la branche du PR anticipé pour continuer et apres push sur la même branche ( *`nous allons en parler un peu plus tard dans la partie `* :six: ). Ceci mettra à jour vôtre branche distante. Mais git ne vous proposera plus de faire un PR. Soyez sans crainte, il sait ce qu'l fait ✅

Allez-y maintenant sur le PR en question (sur gitlab), vous y trouverai un bouton `Mark as ready` comme l'indique l'image suivante et puis c'est bon.

![Mark as ready](/img/mark-as-ready.png "Cliquez sur le bouton Mark as ready")

##### :heavy_minus_sign: Mettre en cache vos fichiers

Cette manière permet de mettre en cache tous vos fichiers et après de les recupérer grâce à une autre commande. Dans vôtre terminal, entrez les commandes suivantes :

```ts
git stash*
```

> ###### Lorsque vous serez de retour sur cette même branche, entrez la commande

```ts
git stash pop
```

Dans les deux cas, après avoir fait le PR ou mise ne cache, déplacez-vous dans la branche dans laquelle l'erreur ou les commentaires ont été signalé par vôtre Master.

Si c'est un conflit qu'il faudra resourdre, entrez la commande suivante : 

```ts
git pull --rebase upstream master_
git status
```

Resolvez vos conflits puis entrez ces conmandes :

```ts
git add .
git rebase --continue
```

> ###### Faites ensuite un PR sur la même branche

### :six: Corrections suivant les commentaires fait sur un PR

Après avoir fait un PR, le validateur de vôtre code (ici, vôtre Master) peut faire des commentaires sur vôtre code. Ces commentaires peuvent concerner vôtre manière de coder, un algorithme particulier, des lignes qui ne servent à rien. Bref, des critiques liés à vôtre code.
Pour corriger ceux-ci, vous devez vous déplacer dans la branche en question, puis entrez la commande suivante :

```ts
git reset HEAD~1
```
> ###### Cette commande est délicate. Toutefois, elle permet d'annuler vôtre dernier `commit` et vous reverez vos fichiers (avec leurs états modifié ou ajouté ) exactement comme ils étaient avant de commiter.

Après modification, référez-vous à l'étape :two: . Renvoyer vôtre code sur la même branche en forçant. 

Ceci n'étant pas exhaustive, le document est donc appelé à évoluer. J'espère tout de même qu'il vous aura appris les bases. N'hésitez surtout pas d'approcher n'importe quel Akiller, il vous aidera avec joie.

##### A vous de jouer : Forker ce projet et exercez-vous :computer:

### :seven: Contributeurs

| Nom & Prenoms |     gitName     |        Rôle |
| :------------ | :-------------: | ----------: |
| KOUAME Yao    |   @kouameYao    |   Rédacteur |
| Hassan        |   @Al-Hassan    | Instructeur |
| Mouctar       |  @mouctarbah6   |    Reviewer |
| Patrice       |    @patrice     | Instructeur |
| Amara         | @amara.dianney  | Instructeur |
| Maxence       | @maxence_yrowah | Instructeur |
| Duverdier     |   duverdier1    | Instructeur |

### :eight: Définition de termes
[^forker]: Il s'agit du processus qui vous permet d'avoir le même projet que vôtre dépôt distant
[^master]: Un master, c'est le supérieur sur un projet. Certaines personnes l'appellent manager
[^wip]: Work In Proccess
[^akiller]: Être un développeur chez [AKILTECHNOLOGIES](https://akiltechnologies.com/)
